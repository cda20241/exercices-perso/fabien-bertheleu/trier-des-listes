liste_principale = [
    {"prenom" : "Pascal", "nom" : "ROSE", "age" : 43},
    {"prenom" : "Mickaël", "nom" : "FLEUR", "age" : 29},
    {"prenom" : "Henri", "nom" : "TULIPE","age" : 35},
    {"prenom" : "Michel", "nom" : "FRAMBOISE", "age" : 35},
    {"prenom" : "Arthur", "nom" : "PETALE", "age" : 35},
    {"prenom" : "Michel", "nom" : "POLLEN", "age" : 50},
    {"prenom" : "Maurice", "nom" : "FRAMBOISE", "age" : 42},
    ]

"""
    je trie dans l'ordre alphabetique la liste en utilisant le nom puis le prénom si le meme prenom
"""
liste_triee = sorted(liste_principale, key=lambda x: (x["nom"], x["prenom"]))
print("## LISTE 1 : tri par ordre alphabetique des noms : ##")
for personne in liste_triee:
        print(personne["prenom"], personne["nom"], personne["age"], "ans")
print("##############################\n")


"""
    Je trie par ordre alphabetique inversé des prénoms
"""
liste_triee_inversee = sorted(liste_principale, key=lambda x: (x["prenom"]), reverse=True)
print("## LISTE 2 : tri par ordre alphabetique inversé des prénoms : ##")
for prenom_inverse in liste_triee_inversee:
    print(prenom_inverse["prenom"], prenom_inverse["nom"], prenom_inverse["age"])
print("##############################\n")

"""
    je trie dans la liste en utilisant l'âge'
"""
liste_triee_par_age = sorted(liste_principale, key=lambda x: (x["age"]))
print("\n## LISTE 3 : Tri par âge croissant :")
for personne_age in liste_triee_par_age  :
        print(personne_age["prenom"], personne_age["nom"], personne_age["age"], "ans")
print("##############################\n")
