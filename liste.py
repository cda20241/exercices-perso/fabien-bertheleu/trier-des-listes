INDEX_PRENOM = 0
INDEX_NOM = 1
INDEX_AGE = 2

liste_principale = [
    ["Pascal","ROSE", 43],
    ["Mickaël", "FLEUR", 29],
    ["Henri", "TULIPE", 35],
    ["Michel", "FRAMBOISE", 35],
    ["Arthur", "PETALE", 35],
    ["Michel", "POLLEN",  50],
    ["Maurice", "FRAMBOISE",  42],
    ]

def tri_par_index(liste,i):
    for f in range(len(liste)-1):
        for e in range(len(liste)-1):
            if liste[e][i] > liste[e+1][i]:
                liste[e], liste[e+1] = liste[e+1], liste[e]
    return liste


def tri_inverse(liste,i):
    for f in range(len(liste)-1):
        for e in range(len(liste)-1):
            if liste[e][i] < liste[e+1][i]:
                liste[e], liste[e+1] = liste[e+1], liste[e]
    return liste

def nom_prenom(liste):
    for f in range(len(liste)-1):
        for e in range(len(liste)-1):
            if liste[e][1] > liste[e+1][1] or (liste[e][1] == liste[e+1][1] and liste[e][0] > liste[e+1][0]):
                liste[e], liste[e+1] = liste[e+1], liste[e]
    return liste


# ## Je créé une liste des noms à partir de la liste principale
# liste_nom = []
# n = 0
# for liste_avec_nom in range(len(liste_principale)):
#     liste_nom.append(liste_principale[n][INDEX_NOM])
#     n = n+1
# print("##### test LISTE QU'AVEC LES NOMS ####\n", (liste_nom))
#


# Execution du code

print("## Liste 1 : tri par ordre alphabétique des noms ##")
for personne in tri_par_index(liste_principale, INDEX_NOM):
    print(personne[0], personne[1], personne[2], " ans.")

print("## Liste 2 : tri par ordre alphabétique des prénoms ##")
for personne in tri_inverse(liste_principale, INDEX_PRENOM):
    print(personne[0], personne[1], personne[2], " ans.")

print("## Liste 3 : tri par ordre croissant des ages ##")
for personne in tri_par_index(liste_principale, INDEX_AGE):
    print(personne[0], personne[1], personne[2], " ans.")

print("## Liste 4 : tri par ordre alphabétique des noms et prénoms ##")
for personne in nom_prenom(liste_principale):
    print(personne[0], personne[1], personne[2], " ans.")



